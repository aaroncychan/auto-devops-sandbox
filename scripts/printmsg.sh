#!/bin/bash
#
# Created by Aaron Chan
# 
# Edit this file to create a push event change thru Gitlab Webhooks
#

PARAM=$1
HOSTNAME=`hostname`

if $PARAM == "poky"; then
  echo "Poky ran successfully"
else
  echo "$HOSTNAME ran successfully"
fi